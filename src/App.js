import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom' 
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { selectCurrentUser } from './redux/user/selector'

import ShopPage from './pages/shop/shop'
import Homepage from './pages/homepage/homepage'
import SigninSignUp from './pages/signin-signup/signin-signup'
import Checkout from './pages/checkout/checkout'

import Header from './components/header/header'

import { auth, createUserProfileDocument } from './firebase/firebase.utils';

import { setCurrentUser } from './redux/user/action'


import './App.css'

class App extends React.Component {

  unsubscribeFromAuth = null

  componentDidMount() {
    const { setCurrentUser } = this.props
    this.unsubscribeFromAuth = auth.onAuthStateChanged( async user => { 
        if(user) {
          const userRef = await createUserProfileDocument(user)
          userRef.onSnapshot( snapshot => {
            setCurrentUser({
                id:snapshot.id,
                ...snapshot.data()
            })
          })
        }
        setCurrentUser(user)
      }
    )
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth = null
  }
  
  render () {
    return (
      <div>
      <Header />
        <Switch>
          <Route exact path='/' component={Homepage}/>
          <Route path='/shop' component={ShopPage} />
          <Route exact path='/checkout' component={Checkout} />
          <Route 
            exact 
            path='/signin'
            render={() => this.props.currentUser ? 
              (<Redirect to='/'/>) : 
              (<SigninSignUp/>) } />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
})

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
})

export default connect(mapStateToProps,mapDispatchToProps)(App);
