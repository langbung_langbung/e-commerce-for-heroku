import React from 'react'
import './custom-button.scss'

const CustomButton = ({children, isGoogleSignIn, inverted, ...buttonProps}) => (
    <button className={`custom-button ${inverted ? 'inverted' : ''} ${isGoogleSignIn ? 'google-sign-in' : ''}`} 
        {...buttonProps}>
        {children}
    </button>
)
export default CustomButton