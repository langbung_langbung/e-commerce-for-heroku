import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { selectShopCollectionsForPreview } from '../../redux/shop/shop.selector'

import CollectionPreview from '../../components/collection-preview/collection-preview'
const CollectionOverview = ({collections,...props}) => (
    <div className='collecion-overview'>
        {
            collections.map(
                ({id, ...collectionPreviewProps}) => (
                        <div key={id}>
                            <CollectionPreview {...collectionPreviewProps}/>
                        </div>
                    )
            )
        }
    </div>
)

const mapStateToProps = createStructuredSelector({
    collections: selectShopCollectionsForPreview
})

export default connect(mapStateToProps)(CollectionOverview)