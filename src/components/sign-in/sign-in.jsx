import React, { Component } from 'react'
import FormInput from '../form-input/form-input'
import CustomButton from '../custom-button/custom-button'
import { auth, signInWithGoogle } from '../../firebase/firebase.utils'

import './sign-in.scss'
class SignIn extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
        }
    }

    handleSubmit = async event => {
        event.preventDefault()
        const { email, password } = this.state
        alert ('handle submit')
        try {
            await auth.signInWithEmailAndPassword(email, password)
            this.setState({email: '', password: ''})
        } catch (err) {
            // alert(
            //     "sign in failed!!!!"
            // )
            console.error('sign in error:',err.message)
        }
    }

    handleChangeInput = event => {
        const { name, value } = event.target
        this.setState({[name]: value})
    }

    render() {
        return (
        <div className='sign-in'>
            <h2 className='title'>I already have an account</h2>
            <span>signin with your email and password</span>
            <form onSubmit={this.handleSubmit}>
                <FormInput 
                    id='email' 
                    name='email' 
                    type='text' 
                    value={this.state.email} 
                    onChange={this.handleChangeInput}
                    label='email'
                />
                <FormInput 
                    id='password' 
                    name='password' 
                    type='password' 
                    value={this.state.password} 
                    onChange={this.handleChangeInput}
                    label='password'
                />
                <div className='sign-in-group'>
                    <CustomButton type='submit' >Sign in</CustomButton>
                    <CustomButton type="button" onClick={signInWithGoogle} isGoogleSignIn>Sign in with Google</CustomButton>
                </div>
                
            </form>
        </div>
        )
    }
}

export default SignIn