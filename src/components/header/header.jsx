import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { selectCartHidden } from '../../redux/cart/selector'
import { selectCurrentUser } from '../../redux/user/selector' 
import { ReactComponent as Logo } from '../../assets/crown.svg'
import { auth } from '../../firebase/firebase.utils'

import CartIcon from '../cart-icon/cart-icon'
import CartDropdown from '../cart-dropdown/cart-dropdown'
import './header.scss'

const Header = ({currentUser, hiddenCart}) => (
    <div className='header'>
        <Link to='/' className='logo-container'>
            <Logo className='logo'/>
        </Link>
        <div className='options'>
            <Link to='/shop' className='option'>Shop</Link>
            <Link to='/contact' className='option'>Contact</Link>
            {
                currentUser ?
                <div className='option' onClick={ () => {auth.signOut()}}>SIGN OUT</div>
                :
                <Link to='/signin' className='option'>SIGN IN</Link>

            }
            <CartIcon />
        </div>
        {
            hiddenCart?
            null :
            <CartDropdown />
        }
    </div>
)

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
    hiddenCart: selectCartHidden
})

export default connect(mapStateToProps)(Header)