import React from 'react'
import { connect } from 'react-redux'

import { selectCartItemsCount } from '../../redux/cart/selector'
import { toggleCart } from '../../redux/cart/action'
import { ReactComponent as ShoppingIcon } from '../../assets/shopping-bag.svg'
import './cart-icon.scss'

const CartIcon = ({ toggleCart,itemsCount }) => (
    <div className='cart-icon' onClick={toggleCart}> 
        <ShoppingIcon className='shopping-icon' />
        <span className='item-count'>{ itemsCount }</span>
    </div>
)

const mapDispatchToProp = dispatch => ({
    toggleCart: () => dispatch(toggleCart())
})

const mapStateToProps = state => ({
    itemsCount: selectCartItemsCount(state)
})

export default connect(mapStateToProps, mapDispatchToProp)(CartIcon)